----------------------------------------------------------------------------------
-- Company	       : 	Grupo Simon Dice
-- Engineers	   : 	Borja Gutierrez de Cabiedes
--          	 	    Diego Machuca Meo�o
--           		    Sergio Martinez Plana
-- Design Name	   : 	Modulo Meta Harden
-- Module Name     : 	Modulo Meta Harden - BEHAVIORAL
-- Project Name    : 	Simon Dice
-- Target Devices  : 	Artix 7 (Nexys 4DDR)
-- Tool Versions   : 	Vivado 2019.1
-- Description     : 
--              	    Evita que las salidas del registro no se conviertan metaestables. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY metaestabilizador is	
GENERIC (
  BIT   : STD_LOGIC := '1' ;                    -- POSITIVE = 1 & NEGATIVE = 0
  WIDTH : INTEGER   := 1
);
PORT ( 
  sig_syn  : in  STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0) ; 
  rst_asyn : in  STD_LOGIC                          ;
  clk_asyn : in  STD_LOGIC                          ;
  sig_asyn : out STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0)
);
end metaestabilizador ;

ARCHITECTURE BEHAVIORAL of metaestabilizador is
-- Signals
  signal metaestable_s : STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0) ;
  signal sig_asyn_s    : STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0) ;
-- Attributes
  attribute ASYNC_REG                  : string             ;
  attribute ASYNC_REG of metaestable_s : signal is "TRUE"   ;
  attribute ASYNC_REG of sig_asyn_s        : signal is "TRUE"   ;    
BEGIN
  sig_asyn <= sig_asyn_s ;

  resynchronization: PROCESS(clk_asyn, rst_asyn)
  BEGIN
    if rst_asyn ='1' then
      metaestable_s <= (others => not(BIT)) ;
      sig_asyn_s    <= (others => not(BIT)) ;
    elsif rising_edge(clk_asyn) then
      metaestable_s <= sig_syn              ;                         
      sig_asyn_s    <= metaestable_s        ;
    end if;
  end PROCESS;

end BEHAVIORAL;
