library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity dbnc_tb is
--  PORT ( );
end dbnc_tb;

ARCHITECTURE BEHAVIORAL of dbnc_tb is
--Components
--------------------------------------------------------------------------------------------------------------------------------------------------
 component dbnc is
 PORT (
   rst      : in    STD_LOGIC ;
   clk      : in    STD_LOGIC ;
   sample   : in    STD_LOGIC ; 			 -- sample signal should be around 8 kHz
   sig      : in    STD_LOGIC ;
   dbnc_sig : out   STD_LOGIC
 );
 end component;
---------------------------------------------------------------------------------------------------------------------------------------------------   
 component delay_cnt is
 GENERIC (
   -- Default is 30M cycles, with a 100MHz clock, which is 300 ms
   SHORT_SIM       : BOOLEAN               := false                     ;
   COUNT_VAL       : UNSIGNED(24 DOWNTO 0) := TO_UNSIGNED(30000000, 25) ;
   COUNT_VAL_SHORT : UNSIGNED(24 DOWNTO 0) := TO_UNSIGNED(2, 25)
 );
 PORT (
   rst          : in    STD_LOGIC ;
   clk          : in    STD_LOGIC ;
   start_delay  : in    STD_LOGIC ;
   end_delay    : out   STD_LOGIC
 );
 end component;
---------------------------------------------------------------------------------------------------------------------------------------------------
--Signals  
  signal rst_s, clk_s, program_start, sample_8khz, sig_test, output : STD_LOGIC     ;
--Constant
  constant clk_period                                               : TIME := 10 ns ;
BEGIN 
  delay_smpl_8kHz : delay_cnt
  GENERIC MAP (
    SHORT_SIM        => false                  ,
    COUNT_VAL        => TO_UNSIGNED(12500, 25) ,
    COUNT_VAL_SHORT  => TO_UNSIGNED(5, 25)
   )
  PORT MAP (
    rst          => rst_s                        ,
    clk          => clk_s                        ,
    start_delay  => program_start or sample_8khz ,
    end_delay    => sample_8khz
  );
  
  uut: dbnc
  PORT MAP (
    rst      => rst_s       ,
    clk      => clk_s       ,
    sample   => sample_8khz ,
    sig      => sig_test    ,
    dbnc_sig => output
  );
---------------------------------------------------------------  
  clk_gen: PROCESS
  BEGIN
    clk_s <= '0' ;
    wait for clk_period/2 ;
    clk_s <= '1' ;
    wait for clk_period/2 ;  
  end PROCESS;
--------------------------------------------------------------- 
-- Activacion program_start y reset
  program_start <= '1', '0' after clk_period;
  rst_s <= '0';
---------------------------------------------------------------  
  test: PROCESS
  BEGIN
     --Weak duration signal
    sig_test <= '1';
    wait for 200*clk_period;
    assert output = '0' 
    report "ERROR: Weak signal not filtered" severity error;
    sig_test <= '0';     
    wait for 100*clk_period;

    --Good signal
    sig_test <= '1';
    wait for 2 ms;     
    assert output = '1'
    report "ERROR: Good signal not detected" severity error;
    sig_test <= '0';
    wait for 1 ms;

    --Good signal with noise
    sig_test <= '1';
    wait for 100*clk_period;
    sig_test <= '0';
    wait for 50*clk_period;
    sig_test <= '1';
    wait for 600 us;
    sig_test <= '0';
    wait for 60*clk_period; 
    sig_test <= '1';
    wait for 400 us;
    assert output = '1'
    report "ERROR: Good noisy signal not detected" severity error;
    sig_test <= '0';
    wait for 1 ms; 

    --END
    assert false
    report "END OF SIMULATION" severity failure;     
  end PROCESS;
---------------------------------------------------------------  
end BEHAVIORAL;