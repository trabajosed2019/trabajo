----------------------------------------------------------------------------------
-- Company	       : 	Grupo Simon Dice
-- Engineers	   : 	Borja Gutierrez de Cabiedes
--          	 	    Diego Machuca Meo�o
--           		    Sergio Martinez Plana
-- Design Name	   : 	Modulo Counter 
-- Module Name     : 	Modulo Counter - BEHAVIORAL
-- Project Name    : 	Simon Dice
-- Target Devices  : 	Artix 7 (Nexys 4DDR)
-- Tool Versions   : 	Vivado 2019.1
-- Description     : 
--              	    Se utiliza para contar tanto la longitud a memorizar como los 
--              	    elementos introducidos de forma incremental.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY cnt is
GENERIC (WIDTH : INTEGER := 8);
PORT (
  clk     : in    STD_LOGIC                          ;
  rst     : in    STD_LOGIC                          ;  					-- sincronizar
  set_1   : in    STD_LOGIC                          ;  					-- set 1
  incr    : in    STD_LOGIC                          ;  					-- incremento
  counter : out   STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0)
);
end cnt;

ARCHITECTURE BEHAVIORAL of cnt is
--Signals
  signal clk_s   : STD_LOGIC                                     ;
  signal cnt_p_s : UNSIGNED(WIDTH-1 DOWNTO 0) := (others => '0') ;
  signal cnt_f_s : UNSIGNED(WIDTH-1 DOWNTO 0)                    ;
BEGIN
  clk_s <= clk;
-----------------------------------------------------------------------
  REGISTERED: PROCESS (clk)
  BEGIN
    if rising_edge(clk) then
      cnt_p_s <= cnt_f_s ;
    end if;
  end PROCESS;
-----------------------------------------------------------------------
  COMBINATORIAL: PROCESS (rst, set_1, incr, cnt_p_s)
  BEGIN
    if (rst = '1') then
      cnt_f_s <= TO_UNSIGNED(0, cnt_f_s'length) ;
    elsif (set_1 = '1') then
      cnt_f_s <= TO_UNSIGNED(1, cnt_f_s'length) ;
    elsif (incr = '1') then
      cnt_f_s <= cnt_p_s + 1                    ;
    else
      cnt_f_s <= cnt_p_s                        ;
    end if;
  end PROCESS;
-----------------------------------------------------------------------
  -- Assignment Outputs
  counter <= STD_LOGIC_VECTOR(cnt_p_s);

end BEHAVIORAL;