----------------------------------------------------------------------------------
-- Company	      : 	Grupo Simon Dice
-- Engineers	  : 	Borja Gutierrez de Cabiedes
--          	 	    Diego Machuca Meo�o
--           		    Sergio Martinez Plana
-- Design Name	  : 	Modulo Maquina de Estados (SIMON)
-- Module Name    : 	Modulo Maquina de Estados (SIMON) - BEHAVIORAL
-- Project Name   : 	Simon Dice
-- Target Devices : 	Artix 7 (Nexys 4DDR)
-- Tool Versions  : 	Vivado 2019.1
-- Description    : 
--              	    Gestiona las secuencias a reproducir por Simon Dice, las longitudes de 
--              	    secuencias tanto al generar como a la espera del usuario.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

ENTITY st_machine_simon is
PORT (
  rst              : in    STD_LOGIC                    ;
  clk              : in    STD_LOGIC                    ;
  start_game       : in    STD_LOGIC                    ;
  key_pressed      : in    STD_LOGIC                    ;
  key_correct      : in    STD_LOGIC                    ;
  serie_length     : in    STD_LOGIC_VECTOR(7 DOWNTO 0) ;
  item_cnt         : in    STD_LOGIC_VECTOR(7 DOWNTO 0) ;
  display_ready    : in    STD_LOGIC                    ;
  next_prbs        : out   STD_LOGIC                    ;
  load_prbs        : out   STD_LOGIC                    ;
  serie_length_1   : out   STD_LOGIC                    ;
  serie_length_inc : out   STD_LOGIC                    ;
  item_cnt_rst     : out   STD_LOGIC                    ;
  item_cnt_inc     : out   STD_LOGIC                    ;
  ms_player        : out   STD_LOGIC                    ;
  ms_gameover      : out   STD_LOGIC                    ;
  ms_digit_valid   : out   STD_LOGIC                    

);
end st_machine_simon;
ARCHITECTURE BEHAVIORAL of st_machine_simon is

    type estado is (inicio, preparar, mostrar, mostrar_aux, esperar_display, comprobar_secuencia, turno_jugador, esperar_jugador, recoger_entrada, esperar_boton, comprobar_entrada,
                    comprobar_secuencia_j, comprobar_secuencia_j_2, nuevo_elemento, fin);
-- Signals
    signal estado_p, estado_f                                                                                                                            : estado           ;
    signal next_prbs_p, load_prbs_p, ms_player_p, ms_gameover_p, ms_digit_valid_p, serie_length_inc_p, serie_length_1_p,  item_cnt_rst_p, item_cnt_inc_p : STD_LOGIC := '0' ;


BEGIN

-- Assignments
    next_prbs        <= next_prbs_p        ;
    load_prbs        <= load_prbs_p        ;
    ms_player        <= ms_player_p        ;
    ms_gameover      <= ms_gameover_p      ;
    ms_digit_valid   <= ms_digit_valid_p   ;
    serie_length_1   <= serie_length_1_p   ;
    serie_length_inc <= serie_length_inc_p ;
    item_cnt_rst     <= item_cnt_rst_p     ;
    item_cnt_inc     <= item_cnt_inc_p     ;
----------------------------------------------------------------------------------------------------------------------------
-- Camnbia de estado acorde al sincronismo
  P_SYNC: PROCESS(clk, rst)
    BEGIN
     if(rst = '1')then
         estado_p         <= inicio            ;
     elsif(clk'event and clk = '1')then
         estado_p         <= estado_f          ;  
     end if;
    end PROCESS;
-----------------------------------------------------------------------------------------------------------------------------
  P_COMB: PROCESS(estado_p, start_game, key_pressed, key_correct, display_ready, serie_length, item_cnt)
    BEGIN 
	case estado_p is
-- INICIO -> PREPARAR   
        when inicio =>
        -- Outputs
            next_prbs_p        <= '0'                     ; 
            load_prbs_p        <= '0'                     ;
            serie_length_1_p   <= '0'                     ;
            serie_length_inc_p <= '0'                     ;
            item_cnt_rst_p     <= '0'                     ;
            item_cnt_inc_p     <= '0'                     ;
            ms_player_p        <= '0'                     ;
            ms_gameover_p      <= '0'                     ;
            ms_digit_valid_p     <= '0'                     ;
        -- Transition
            if start_game = '0' then
                estado_f       <= inicio                  ;               
            else
                estado_f       <= preparar                ;                
            end if; 
-- PREPARAR -> MOSTRAR  
        when preparar =>
        -- Outputs
            next_prbs_p        <= '0'                     ;
            load_prbs_p        <= '1'                     ;
            serie_length_1_p   <= '1'                     ;
            serie_length_inc_p <= '0'                     ;
            item_cnt_rst_p     <= '1'                     ;
            item_cnt_inc_p     <= '0'                     ;
            ms_player_p        <= '0'                     ;
            ms_gameover_p      <= '0'                     ;
            ms_digit_valid_p   <= '0'                     ; 
        -- Transition
            if display_ready = '0' then
                estado_f       <= preparar                ;               
            else
                estado_f       <= mostrar                 ;                
            end if;
-- MOSTRAR -> ESPERAR_DISPLAY           
        when mostrar =>  
        -- Outputs
            next_prbs_p        <= '1'                     ;
            load_prbs_p        <= '0'                     ;
            serie_length_1_p   <= '0'                     ;
            serie_length_inc_p <= '0'                     ;
            item_cnt_rst_p     <= '0'                     ;
            item_cnt_inc_p     <= '1'                     ;
            ms_player_p        <= '0'                     ;
            ms_gameover_p      <= '0'                     ;
            ms_digit_valid_p   <= '0'                     ;
        -- Transition
            estado_f           <= esperar_display         ;  
-- ESPERAR_DISPLAY -> COMPROBAR_SECUENCIA               
        when esperar_display => 
        -- Outputs
            next_prbs_p        <= '0'                     ;
            load_prbs_p        <= '0'                     ;
            serie_length_1_p   <= '0'                     ;
            serie_length_inc_p <= '0'                     ;
            item_cnt_rst_p     <= '0'                     ;
            item_cnt_inc_p     <= '0'                     ;
            ms_player_p        <= '0'                     ;
            ms_gameover_p      <= '0'                     ;
            ms_digit_valid_p   <= '1'                     ;
        -- Transition
            if display_ready = '0' then
                estado_f       <= esperar_display         ;               
            else
                estado_f       <= comprobar_secuencia     ;                
            end if;
-- COMPROBAR_SECUENCIA => MOSTRAR_AUX -> TURNO_JUGADOR
        when comprobar_secuencia =>
        -- Outputs
            next_prbs_p        <= '0'                     ;
            load_prbs_p        <= '0'                     ;
            serie_length_1_p   <= '0'                     ;
            serie_length_inc_p <= '0'                     ;
            item_cnt_rst_p     <= '0'                     ;
            item_cnt_inc_p     <= '0'                     ;
            ms_player_p        <= '0'                     ;
            ms_gameover_p      <= '0'                     ;
            ms_digit_valid_p   <= '0' 		              ;
        -- Transition
            if item_cnt/= serie_length then
                estado_f       <= mostrar_aux		      ;               
            else
                estado_f       <= turno_jugador	          ;                
            end if; 
        when mostrar_aux =>
-- MOSTRAR_AUX -> MOSTRAR                                     -- Activate next_prbs output during 2 periods => OK
        -- Outputs
            next_prbs_p        <= '1' 		              ;
            load_prbs_p        <= '0' 		              ;
            serie_length_1_p   <= '0' 		              ;
            serie_length_inc_p <= '0' 		              ;
            item_cnt_rst_p     <= '0' 		              ;
            item_cnt_inc_p     <= '0' 		              ;
            ms_player_p        <= '0' 		              ;
            ms_gameover_p      <= '0' 		              ;
            ms_digit_valid_p   <= '0' 		              ;
        -- Transition
            estado_f   	       <= mostrar                 ;
-- TURNO_JUGADOR -> ESPERAR_JUGADOR
        when turno_jugador =>
        -- Outputs
            next_prbs_p        <= '0' 		              ;
            load_prbs_p        <= '1' 		              ;
            serie_length_1_p   <= '0'		              ;
            serie_length_inc_p <= '0' 		              ;
            item_cnt_rst_p     <= '1' 		              ;
            item_cnt_inc_p     <= '0' 		              ;
            ms_player_p        <= '1' 		              ;
            ms_gameover_p      <= '0' 		              ;
            ms_digit_valid_p   <= '0' 		              ;  
        -- Transition
            estado_f           <= esperar_jugador         ;
-- ESPERAR_JUGADOR -> RECOGER_ENTRADA
        when esperar_jugador =>
        -- Outputs
            next_prbs_p        <= '0' 		              ;
            load_prbs_p        <= '0' 		              ;
            serie_length_1_p   <= '0' 		              ;
            serie_length_inc_p <= '0' 		              ;
            item_cnt_rst_p     <= '0' 		              ;
            item_cnt_inc_p     <= '0' 		              ;
            ms_player_p        <= '1' 		              ;
            ms_gameover_p      <= '0' 		              ;
            ms_digit_valid_p   <= '0' 		              ;
        -- Transition
            if key_pressed = '0' then
                estado_f       <= esperar_jugador         ;               
            else
                estado_f       <= recoger_entrada         ;                
            end if; 
-- RECOGER_ENTRADA -> COMPROBAR_ENTRADA 
        when recoger_entrada =>
        -- Outputs
            next_prbs_p        <= '0' 		              ;
            load_prbs_p        <= '0' 		              ;
            serie_length_1_p   <= '0' 		              ;
            serie_length_inc_p <= '0' 		              ;
            item_cnt_rst_p     <= '0' 		              ;
            item_cnt_inc_p     <= '1' 		              ;
            ms_player_p        <= '1' 		              ;
            ms_gameover_p      <= '0' 		              ;
            ms_digit_valid_p     <= '0' 		              ;
        -- Transition
            estado_f 	       <= comprobar_entrada       ;
-- COMPROBAR_ENTRADA => ESPERAR_BOTON o FIN
        when comprobar_entrada =>
        -- Outputs
            next_prbs_p        <= '0' 		              ;
            load_prbs_p        <= '0' 		              ;
            serie_length_1_p   <= '0' 		              ;
            serie_length_inc_p <= '0' 		              ;
            item_cnt_rst_p     <= '0' 		              ;	
            item_cnt_inc_p     <= '0' 		              ;
            ms_player_p        <= '1' 		              ;
            ms_gameover_p      <= '0' 		              ;
            ms_digit_valid_p   <= '0' 		              ;
        -- Transition
            if key_correct = '1' then
                estado_f       <= esperar_boton	          ;               
            else
                estado_f       <= fin 		              ;                
            end if; 
-- ESPERAR_BOTON -> COMBROBAR_SECUENCIA_J
        when esperar_boton =>       
        -- Outputs
            next_prbs_p        <= '0' 		              ;
            load_prbs_p        <= '0' 		              ;
            serie_length_1_p   <= '0' 		              ;
            serie_length_inc_p <= '0' 		              ;
            item_cnt_rst_p     <= '0' 		              ;
            item_cnt_inc_p     <= '0' 		              ;
            ms_player_p        <= '1' 		              ;
            ms_gameover_p      <= '0' 		              ; 
            ms_digit_valid_p   <= '0' 		              ;
        -- Transition
            if key_pressed = '1' then
                estado_f       <= esperar_boton	          ;               
            else
                estado_f       <= comprobar_secuencia_j   ;                
            end if;
-- COMBROBAR_SECUENCIA_J => COMBROBAR_SECUENCIA_J_2 o NUEVO_ELEMENTO         
        when comprobar_secuencia_j =>
        -- Outputs
            next_prbs_p        <= '1' 		              ;
            load_prbs_p        <= '0' 		              ;
            serie_length_1_p   <= '0' 		              ;
            serie_length_inc_p <= '0' 		              ;
            item_cnt_rst_p     <= '0'                     ;
            item_cnt_inc_p     <= '0' 		              ;
            ms_player_p        <= '1'  		              ;
            ms_gameover_p      <= '0' 	 	              ;
            ms_digit_valid_p   <= '0' 		              ;
        -- Transition
            if item_cnt/= serie_length then
                estado_f       <= comprobar_secuencia_j_2 ;               
            else
                estado_f       <= nuevo_elemento	      ;                
            end if; 
-- COMBROBAR_SECUENCIA_J_2 -> ESPERAR_JUGADOR      
        when comprobar_secuencia_j_2 =>
        -- Outputs
            next_prbs_p        <= '1'                     ;
            load_prbs_p        <= '0'                     ;
            serie_length_1_p   <= '0'                     ;
            serie_length_inc_p <= '0'                     ;
            item_cnt_rst_p     <= '0'                     ;
            item_cnt_inc_p     <= '0'                     ;
            ms_player_p        <= '1'                     ;
            ms_gameover_p      <= '0'                     ;
            ms_digit_valid_p   <= '0'                     ;
        -- Transition
            estado_f           <= esperar_jugador         ;
-- NUEVO_ELEMENTO -> MOSTRAR   
        when nuevo_elemento =>
        -- Outputs
            next_prbs_p        <= '0'                     ;
            load_prbs_p        <= '1'                     ;
            serie_length_1_p   <= '0'                     ;
            serie_length_inc_p <= '1'                     ;
            item_cnt_rst_p     <= '1'                     ;
            item_cnt_inc_p     <= '0'                     ;
            ms_player_p        <= '0'                     ;
            ms_gameover_p      <= '0'                     ;
            ms_digit_valid_p   <= '0'                     ;
        -- Transition
            estado_f           <= mostrar		          ;   
        when others =>                                                    -- end game state
          -- Outputs
            next_prbs_p        <= '0'                     ;
            load_prbs_p        <= '0'                     ;
            serie_length_1_p   <= '0'                     ;
            serie_length_inc_p <= '0'                     ;
            item_cnt_rst_p     <= '0'                     ;
            item_cnt_inc_p     <= '0'                     ;
            ms_player_p        <= '0'                     ;
            ms_gameover_p      <= '1'                     ;
            ms_digit_valid_p   <= '0'                     ;          
    end case;
 end PROCESS; 

end BEHAVIORAL;