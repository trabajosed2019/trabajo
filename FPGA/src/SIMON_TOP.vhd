----------------------------------------------------------------------------------
-- Company	      : 	Grupo Simon Dice
-- Engineers	  : 	Borja Gutierrez de Cabiedes
--          	 	    Diego Machuca Meo�o
--           		    Sergio Martinez Plana
-- Design Name	  : 	Modulo Simon Top
-- Module Name    : 	Modulo Simon Top - BEHAVIORAL
-- Project Name   : 	Simon Dice
-- Target Devices : 	Artix 7 (Nexys 4DDR)
-- Tool Versions  : 	Vivado 2019.1
-- Description    : 
--              	    Top de Simon
----------------------------------------------------------------------------------
library IEEE;
library UNISIM;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use UNISIM.VCOMPONENTS.ALL;

ENTITY SIMON_TOP is
GENERIC (SHORT_SIM : BOOLEAN := false) ;
PORT (
  rst_n      : in    STD_LOGIC		              ;
  clk_100mhz : in    STD_LOGIC		              ;    
  start_game : in    STD_LOGIC                    ;
  btn        : in    STD_LOGIC_VECTOR(3 DOWNTO 0) ;
  seed       : in    STD_LOGIC_VECTOR(6 DOWNTO 0) ;
  leds       : out   STD_LOGIC_VECTOR(7 DOWNTO 0)
);
end SIMON_TOP;

ARCHITECTURE BEHAVIORAL of SIMON_TOP is
-- Signals
  signal rst_s               : STD_LOGIC                    ;
  signal clk_s               : STD_LOGIC                    ;
  signal sample_8khz_s       : STD_LOGIC                    ;
  signal pulso_s             : STD_LOGIC                    ;
  signal start_game_clk_s    : STD_LOGIC                    ;
  signal btn_clk_s           : STD_LOGIC_VECTOR(3 DOWNTO 0) ;
  signal dbnc_start_game_s   : STD_LOGIC                    ;
  signal dbnc_btn_s          : STD_LOGIC_VECTOR(3 DOWNTO 0) ;
  signal encod_digit_s       : STD_LOGIC_VECTOR(1 DOWNTO 0) ;
  signal encod_key_pressed_s : STD_LOGIC                    ;
  signal key_correct_s       : STD_LOGIC                    ;
  signal serie_length_1_s    : STD_LOGIC                    ;
  signal serie_length_inc_s  : STD_LOGIC                    ;
  signal serie_length_s      : STD_LOGIC_VECTOR(7 DOWNTO 0) ;
  signal item_cnt_rst_s      : STD_LOGIC                    ;
  signal item_cnt_inc_s      : STD_LOGIC			        ;
  signal item_cnt_s          : STD_LOGIC_VECTOR(7 DOWNTO 0) ;
  signal prbs_digit_s        : STD_LOGIC_VECTOR(1 DOWNTO 0) ;
  signal correct_s           : STD_LOGIC                    ;
  signal load_prbs_s         : STD_LOGIC                    ;
  signal next_prbs_s         : STD_LOGIC                    ;
  signal ms_player_s         : STD_LOGIC                    ;
  signal ms_gameover_s       : STD_LOGIC                    ;
  signal ms_digit_valid_s    : STD_LOGIC                    ;
  signal display_ready_s     : STD_LOGIC := '0'             ;
  signal start_delay_s       : STD_LOGIC := '0'             ;
  signal end_delay_s         : STD_LOGIC := '0'             ;
  signal md_player_s         : STD_LOGIC := '0'             ;
  signal md_gameover_s       : STD_LOGIC := '0'             ;
  signal md_serie_s          : STD_LOGIC_VECTOR(3 DOWNTO 0) ;
  signal lumi_serie_s        : STD_LOGIC_VECTOR(3 DOWNTO 0) ;
  signal program_start_s     : STD_LOGIC                    ;
--------------------------------------------------------------------------------------------------------------------------------------------------------------
--Components
--------------------------------------------------------------------------------------------------------------------------------------------------------------
 component metaestabilizador is	
 GENERIC (
   BIT   : STD_LOGIC := '1' ;                    -- POSITIVE = 1 & NEGATIVE = 0
   WIDTH : INTEGER   := 1
 );
 PORT ( 
  sig_syn  : in  STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0) ; 
  rst_asyn : in  STD_LOGIC                          ;
  clk_asyn : in  STD_LOGIC                          ;
  sig_asyn : out STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0)
 );
 end component;
-------------------------------------------------------------------------------------------------------------------------------------------------------------
 component dbnc is
 PORT (
   rst      : in    STD_LOGIC ;
   clk      : in    STD_LOGIC ;
   sample   : in    STD_LOGIC ; 			 -- sample signal should be around 8 kHz
   sig      : in    STD_LOGIC ;
   dbnc_sig : out   STD_LOGIC
 );
 end component;
--------------------------------------------------------------------------------------------------------------------------------------------------------------
 component prbs7 is
 PORT (
   clk       : in    STD_LOGIC		              ;
   load_prbs : in    STD_LOGIC	                  ;
   next_prbs : in    STD_LOGIC	                  ;
   seed      : in    STD_LOGIC_VECTOR(6 DOWNTO 0) ;
   digit     : out   STD_LOGIC_VECTOR(1 DOWNTO 0)
 );
 end component;
---------------------------------------------------------------------------------------------------------------------------------------------------------------
 component cnt is
 GENERIC (WIDTH : INTEGER := 8);
 PORT (
   clk     : in    STD_LOGIC                          ;
   rst     : in    STD_LOGIC                          ;  					-- sincronizar
   set_1   : in    STD_LOGIC                          ;  					-- set 1
   incr    : in    STD_LOGIC                          ;  					-- incremento
   counter : out   STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0)
 );
 end component;
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
 component st_machine_simon is
 PORT (
  rst              : in    STD_LOGIC                    ;
  clk              : in    STD_LOGIC                    ;
  start_game       : in    STD_LOGIC                    ;
  key_pressed      : in    STD_LOGIC                    ;
  key_correct      : in    STD_LOGIC                    ;
  serie_length     : in    STD_LOGIC_VECTOR(7 DOWNTO 0) ;
  item_cnt         : in    STD_LOGIC_VECTOR(7 DOWNTO 0) ;
  display_ready    : in    STD_LOGIC                    ;
  next_prbs        : out   STD_LOGIC                    ;
  load_prbs        : out   STD_LOGIC                    ;
  serie_length_1   : out   STD_LOGIC                    ;
  serie_length_inc : out   STD_LOGIC                    ;
  item_cnt_rst     : out   STD_LOGIC                    ;
  item_cnt_inc     : out   STD_LOGIC                    ;
  ms_player        : out   STD_LOGIC                    ;
  ms_gameover      : out   STD_LOGIC                    ;
  ms_digit_valid   : out   STD_LOGIC                     
 );
 end component; 
----------------------------------------------------------------------------------------------------------------------------------------------------------------
 component st_machine_display is
 PORT (
   rst           : in    STD_LOGIC                    ;
   clk           : in    STD_LOGIC                    ;
   player        : in    STD_LOGIC                    ;
   gameover      : in    STD_LOGIC                    ;
   digit_valid   : in    STD_LOGIC                    ;
   digit         : in    STD_LOGIC_VECTOR(1 DOWNTO 0) ;
   end_delay     : in    STD_LOGIC                    ;
   start_delay   : out   STD_LOGIC                    ;
   display_ready : out   STD_LOGIC                    ;
   md_player     : out   STD_LOGIC                    ;
   md_gameover   : out   STD_LOGIC                    ;
   md_serie      : out   STD_LOGIC_VECTOR(3 DOWNTO 0)
 );
 end component;
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
 component delay_cnt is
 GENERIC (
   -- Default is 30M cycles, with a 100MHz clock, which is 300 ms
   SHORT_SIM       : BOOLEAN               := false                     ;
   COUNT_VAL       : UNSIGNED(24 DOWNTO 0) := TO_UNSIGNED(30000000, 25) ;
   COUNT_VAL_SHORT : UNSIGNED(24 DOWNTO 0) := TO_UNSIGNED(2, 25)
 );
 PORT (
   rst         : in    STD_LOGIC ;
   clk         : in    STD_LOGIC ;
   start_delay : in    STD_LOGIC ;
   end_delay   : out   STD_LOGIC
 );
 end component;
------------------------------------------------------------------------------------------------------------------------------------------------------------------
 component iluminacion is
 GENERIC (
   -- The brightness is defined by the number of active cycles and consequently the parameters are established.
   RATIO_16 : UNSIGNED(3 DOWNTO 0) := X"2" ;
   WIDTH    : INTEGER              := 4
 );
 PORT (
   clk        : in    STD_LOGIC                          ;
   serie      : in    STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0) ;
   lumi_serie : out   STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0)
 );
 end component;
-----------------------------------------------------------------------------------------------------------------------------------------------------------------
 component pulso is
 GENERIC (
   -- A 2 Hz heartbeat with 50% duty cycle on a 100 MHz clock
   COUNT_VAL : UNSIGNED(26 DOWNTO 0) := resize(d"25000000", 27)
 );
 PORT (
   clk   : in    STD_LOGIC ;
   pulso : out   STD_LOGIC
 );
 end component;
---------------------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN
----------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Instantiations
----------------------------------------------------------------------------------------------------------------------------------------------------------------
  -- Clock and reset
  -- Instantaneous BUFG for clock distribution
  -- Adjustment of reset polarity
    BUFG_inst : BUFG 
    PORT MAP (
         O => clk_s,            -- 1-bit output: Clock output   
         I => clk_100mhz        -- 1-bit input: Clock input 
     );
     rst_s           <= not(rst_n)          ;
     program_start_s <= '1', '0' after 10ns ;    
-- End of BUFG_inst instantiation  
----------------------------------------------------------------------------------------------------------------------------------------------------------------
  resynchronization_inst : metaestabilizador
  GENERIC MAP (WIDTH => 5)
  PORT MAP (
    sig_syn(4)            => start_game       ,
    sig_syn (3 DOWNTO 0)  => btn              ,
    rst_asyn              => rst_s            ,
    clk_asyn              => clk_s            ,
    sig_asyn (4)          => start_game_clk_s ,
    sig_asyn (3 DOWNTO 0) => btn_clk_s
  );
--------------------------------------------------------------------------------------------------------------------------------------------------------------
  dbnc_start_game_inst : dbnc
  PORT MAP (
    rst        => rst_s		        ,
    clk        => clk_s		        ,
    sample     => sample_8khz_s     ,
    sig        => start_game_clk_s  ,
    dbnc_sig   => dbnc_start_game_s
  );  
  DBNC_GEN: 
  for W in 0 to 3 generate
    dbnc_btn_inst : dbnc
    PORT MAP (
      rst      => rst_s	          ,
      clk      => clk_s           ,
      sample   => sample_8khz_s   ,
      sig      => btn_clk_s(W)    ,
      dbnc_sig => dbnc_btn_s(W)
    );
  end generate DBNC_GEN;
--------------------------------------------------------------------------------------------------------------------------------------------------------------
  prbs7_inst : prbs7
  PORT MAP (
      clk       => clk_s         ,
      load_prbs => load_prbs_s   ,
      next_prbs => next_prbs_s   ,
      seed      => seed          ,
      digit     => prbs_digit_s
);
---------------------------------------------------------------------------------------------------------------------------------------------------------------
  st_machine_simon_inst : st_machine_simon
    PORT MAP (
      rst              => rst_s               ,
      clk              => clk_s               ,
      start_game       => dbnc_start_game_s   ,
      key_pressed      => encod_key_pressed_s ,
      key_correct      => key_correct_s       ,
      serie_length     => serie_length_s      ,
      item_cnt         => item_cnt_s          ,
      display_ready    => display_ready_s     ,
      next_prbs        => next_prbs_s         ,
      load_prbs        => load_prbs_s         ,
      serie_length_1   => serie_length_1_s    ,
      serie_length_inc => serie_length_inc_s  ,
      item_cnt_rst     => item_cnt_rst_s      ,
      item_cnt_inc     => item_cnt_inc_s      ,
      ms_player        => ms_player_s         ,
      ms_gameover      => ms_gameover_s       ,
      ms_digit_valid   => ms_digit_valid_s    

    );    
  serie_length_inst : cnt
   PORT MAP (
     clk     => clk_s  	           ,
     rst     => '0'   	           ,
     set_1   => serie_length_1_s   ,
     incr    => serie_length_inc_s ,
     counter => serie_length_s
   );
  item_cnt_inst : cnt
   PORT MAP (
     clk     => clk_s          ,
     rst     => item_cnt_rst_s ,
     set_1   => '0'            ,
     incr    => item_cnt_inc_s ,
     counter => item_cnt_s
   );
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -- Encoder 4 to 2
  encod_digit_s        <= "00" when dbnc_btn_s = "0001" or dbnc_btn_s = "0000" else
                          "01" when dbnc_btn_s = "0010" else  
                          "10" when dbnc_btn_s = "0100" else
                          "11" when dbnc_btn_s = "1000" else                        
                          "--";                        

  encod_key_pressed_s  <= '1' when dbnc_btn_s = "0010" or  
                                   dbnc_btn_s = "0100" or
                                   dbnc_btn_s = "1000" or
                                   dbnc_btn_s = "0001" else
                          '0';     
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
  st_machine_display_inst : st_machine_display
   PORT MAP (
     rst            => rst_s            ,
     clk            => clk_s            ,
     player         => ms_player_s      ,
     gameover       => ms_gameover_s    ,
     digit_valid    => ms_digit_valid_s ,
     digit          => prbs_digit_s     ,
     start_delay    => start_delay_s    ,
     end_delay      => end_delay_s      ,
     display_ready  => display_ready_s  ,
     md_player      => md_player_s      ,
     md_gameover    => md_gameover_s    ,
     md_serie       => md_serie_s
   );
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
  delay_cnt_inst : delay_cnt
  GENERIC MAP (
    SHORT_SIM       => SHORT_SIM                 ,
    COUNT_VAL       => TO_UNSIGNED(30000000, 25) ,
    COUNT_VAL_SHORT => TO_UNSIGNED(5, 25)
   )
  PORT MAP (
    rst         => rst_s         ,
    clk         => clk_s         ,
    start_delay => start_delay_s ,
    end_delay   => end_delay_s
  ); 
----------------------------------------------------------------------------------------------------------------------------------------------------------------
  delay_smpl_8kHz : delay_cnt
  GENERIC MAP (
    SHORT_SIM       => SHORT_SIM              ,
    COUNT_VAL       => TO_UNSIGNED(12500, 25) ,
    COUNT_VAL_SHORT => TO_UNSIGNED(5, 25)
   )
  PORT MAP (
    rst         => rst_s                            ,
    clk         => clk_s                            ,
    start_delay => program_start_s or sample_8khz_s ,
    end_delay   => sample_8khz_s
  );
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
  iluminacion_inst : iluminacion
  PORT MAP (
    clk        => clk_s        ,
    serie      => md_serie_s   ,
    lumi_serie => lumi_serie_s  
  );
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
  pulso_inst : pulso
  PORT MAP (
    clk   => clk_s          ,
    pulso => pulso_s
  );
-------------------------------------------------------------------------------------------------------------------------------------------------------------------  
  -- Glue logic
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
    key_correct_s         <= '1' when encod_digit_s = prbs_digit_s else
                             '0';
------------------------------------------------------------------------------------------------------------------------------------------------------------------
  -- Assignment Outputs
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
  leds(7)          <= pulso_s        ;
  leds(6)          <= md_player_s    ;
  leds(5)          <= md_gameover_s  ;
  leds(4)          <= '0'            ;             -- Tie low, inactive
  leds(3 downto 0) <= lumi_serie_s   ;

end BEHAVIORAL;