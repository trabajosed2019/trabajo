----------------------------------------------------------------------------------
-- Company	      : 	Grupo Simon Dice
-- Engineers	  : 	Borja Gutierrez de Cabiedes
--          	 	    Diego Machuca Meo�o
--           		    Sergio Martinez Plana
-- Design Name	  : 	Modulo Delay Counter
-- Module Name    : 	Modulo Delay Counter - BEHAVIORAL
-- Project Name   : 	Simon Dice
-- Target Devices : 	Artix 7 (Nexys 4DDR)
-- Tool Versions  : 	Vivado 2019.1
-- Description    : 
--              		Se utiliza para generar un retraso para mostrar los elementos leds 
--                      para dar tiempo al usurio a repetirlo.     
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY delay_cnt is
GENERIC (
  -- Default is 30M cycles, with a 100MHz clock, which is 300 ms
  SHORT_SIM       : BOOLEAN               := false                     ;
  COUNT_VAL       : UNSIGNED(24 DOWNTO 0) := TO_UNSIGNED(30000000, 25) ;
  COUNT_VAL_SHORT : UNSIGNED(24 DOWNTO 0) := TO_UNSIGNED(2, 25)
);
PORT (
  rst         : in    STD_LOGIC ;
  clk         : in    STD_LOGIC ;
  start_delay : in    STD_LOGIC ;
  end_delay   : out   STD_LOGIC
);
end delay_cnt;

ARCHITECTURE BEHAVIORAL of delay_cnt is
--Signals
    signal cnt  : INTEGER   := 0   ;
    signal flag : STD_LOGIC := '0' ;
BEGIN
  PROCESS (clk, rst, start_delay)
  BEGIN 
    if (rst = '1')then
        end_delay <='0'                   ;    
    else
        if start_delay = '1' then
            flag <= '1'                   ;
        end if;    
        if  rising_edge(clk) then
            if flag = '1' then
                if (cnt = COUNT_VAL) then
                    end_delay <= '1'      ;
                    cnt       <= 0        ;
                    flag      <= '0'      ; 
                 else
                    cnt       <= cnt + 1  ;
                    end_delay <= '0'      ;
                    flag      <= '1'      ;
                 end if;
             else
                  end_delay   <= '0'            ;   
             end if;   
        end if;
    end if;
  end PROCESS;  

end BEHAVIORAL;