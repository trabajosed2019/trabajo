library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY st_machine_display_tb is
-- PORT ();
end st_machine_display_tb;

ARCHITECTURE BEHAVIORAL of st_machine_display_tb is
--Component
 component st_machine_display is
 PORT (
   rst           : in    STD_LOGIC                    ;
   clk           : in    STD_LOGIC                    ;
   player        : in    STD_LOGIC                    ;
   gameover      : in    STD_LOGIC                    ;
   digit_valid   : in    STD_LOGIC                    ;
   digit         : in    STD_LOGIC_VECTOR(1 DOWNTO 0) ;
   end_delay     : in    STD_LOGIC                    ;
   start_delay   : out   STD_LOGIC                    ;
   display_ready : out   STD_LOGIC                    ;
   md_player     : out   STD_LOGIC                    ;
   md_gameover   : out   STD_LOGIC                    ;
   md_serie      : out   STD_LOGIC_VECTOR(3 DOWNTO 0)
 );
 end component;
--Signals
  signal rst_s, clk_s, player_s, gameover_s, digit_valid_s                       : STD_LOGIC                              ;
  signal end_delay_s, start_delay_s, display_ready_s, md_player_s, md_gameover_s : STD_LOGIC                              ;
  signal digit_s                                                                 : STD_LOGIC_VECTOR (1 DOWNTO 0)          ;
  signal md_serie_s                                                              : STD_LOGIC_VECTOR (3 DOWNTO 0)          ;
--Constant
  constant clk_period                                                            : TIME                          := 10 ns ;
BEGIN
uut: st_machine_display
PORT MAP (
   rst           => rst_s           ,
   clk           => clk_s           ,
   player        => player_s        ,
   gameover      => gameover_s      ,
   digit_valid   => digit_valid_s   ,
   digit         => digit_s         ,
   end_delay     => end_delay_s     ,
   start_delay   => start_delay_s   ,
   display_ready => display_ready_s ,
   md_player     => md_player_s     ,
   md_gameover   => md_gameover_s   ,
   md_serie      => md_serie_s
);
---------------------------------------------------------------------------------
  clk_gen: PROCESS
  BEGIN
    clk_s <= '0' ;
    wait for clk_period/2 ;
    clk_s <= '1' ;
    wait for clk_period/2 ;  
  end PROCESS;
---------------------------------------------------------------------------------  
--RESET
rst_s <= '1' after 0.25 * clk_period ,'0' after 1.75 * clk_period ;  
----------------------------------------------------------------------------------
  stimulus: PROCESS
  BEGIN
-- READY 
  wait for  clk_period ;
  digit_valid_s <= '0'  ;
  end_delay_s   <= '0'  ;
  digit_s       <= "00" ;
-- READY -> HIGHLIGHT
 wait for  clk_period ;
  digit_valid_s <= '1'  ;
  player_s      <= '1'  ;
  gameover_s    <= '0'  ;
  digit_s       <= "01" ;
-- HIGHLIGHT -> WAIT_HG
-- WAIT_HG -> PAUSE
 wait for  clk_period ;
  digit_valid_s <= '1'  ;
  end_delay_s   <= '1'  ;
  player_s      <= '1'  ;
  gameover_s    <= '0'  ;
  digit_s       <= "10" ;
-- PAUSE -> WAIT_PAUSE
-- WAIT_PAUSE -> READY
 wait for  clk_period ;
  digit_valid_s <= '1'  ;
  end_delay_s   <= '1'  ;
  player_s      <= '1'  ;
  gameover_s    <= '0'  ;
  digit_s       <= "11" ;
-- READY
 wait for  clk_period ;
  digit_valid_s <= '0'  ;
  end_delay_s   <= '0'  ;
  player_s      <= '0'  ;
  gameover_s    <= '1'  ;
 assert false
 report "[SUCCESS]: simulation finished."
 severity failure;
  end PROCESS;

end BEHAVIORAL;