library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY metaestabilizador_tb is
-- PORT ();
end metaestabilizador_tb;

ARCHITECTURE BEHAVIORAL of metaestabilizador_tb is
-- Component
 component metaestabilizador is	
 GENERIC (
   BIT    : STD_LOGIC := '1' ;                    -- POSITIVE = 1 & NEGATIVE = 0
   WIDTH  : INTEGER   := 1
 );
 PORT ( 
   sig_syn  : in  STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0)  ;
   rst_asyn : in  STD_LOGIC                          ;
   clk_asyn : in  STD_LOGIC                          ;
   sig_asyn : out STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0)
 );
 end component;
--Signals
  signal rst_s, clk_s          : STD_LOGIC                                   ;
  signal sig_syn_s, sig_asyn_s : STD_LOGIC_VECTOR(0 DOWNTO 0)                ;
--Constant
  constant clk_period          : TIME                               := 10 ns ;
BEGIN
  uut: metaestabilizador
  PORT MAP (
     sig_syn  => sig_syn_s   , 
     rst_asyn => rst_s       ,                        
     clk_asyn => clk_s       ,
     sig_asyn => sig_asyn_s
  );
-----------------------------------------------------------------------------  
  clk_gen: PROCESS
  BEGIN
     clk_s <= '0' ;
     wait for clk_period/2 ;
     clk_s <= '1' ;
     wait for clk_period/2 ;  
  end PROCESS;
-----------------------------------------------------------------------------
--RESET
rst_s <= '1', '0' after 3*clk_period ;
-----------------------------------------------------------------------------
  stimulus: PROCESS
  BEGIN
    sig_syn_s <= "0";
    wait for 2*clk_period;
    sig_syn_s <= "1";
    wait for 2*clk_period;
    sig_syn_s <= "0";
    wait for 2*clk_period;
    sig_syn_s <= "1";
  end PROCESS;
----------------------------------------------------------------------------
end BEHAVIORAL;