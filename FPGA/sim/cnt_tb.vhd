library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY cnt_tb is
-- PORT();
end cnt_tb;

ARCHITECTURE BEHAVIORAL of cnt_tb is
--Component
 component cnt is
  GENERIC (WIDTH : INTEGER := 8);
  PORT (
    clk     : in    STD_LOGIC                          ;
    rst     : in    STD_LOGIC                          ;  					-- sincronizar
    set_1   : in    STD_LOGIC                          ;  					-- set 1
    incr    : in    STD_LOGIC                          ;  					-- incremento
    counter : out   STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0)
  );
  end component;
--Signals
  signal clk_s, rst_s, set_1_s, incr_s : STD_LOGIC                              ;
  signal cnt_s                         : STD_LOGIC_VECTOR (7 DOWNTO 0)          ;
--Constant
  constant clk_period                  : TIME                          := 10 ns ;
BEGIN
    uut: cnt
    PORT MAP (
	clk     => clk_s   ,
        rst     => rst_s   ,
        set_1   => set_1_s ,
        incr    => incr_s  ,
        counter => cnt_s 
     );
    rst_s <= '0';
-----------------------------------------------    
    clk_gen: PROCESS
    BEGIN
        clk_s <= '1' ;
        wait for clk_period/2 ;
        clk_s <= '0' ;
        wait for clk_period/2 ;
    end PROCESS;
----------------------------------------------
--RESET
 rst_s <= '1', '0' after 3*clk_period;
----------------------------------------------
    stimulus: PROCESS
    BEGIN
-- Activacion de incremento
        set_1_s <= '0' ;
        incr_s  <= '1' ;        
        wait for 3*clk_period;
-- Activacion de set_1
        set_1_s <= '1' ;
        incr_s  <= '0' ;
        wait for 2*clk_period;
-- Puesta a 0 de incremento y set_1
        set_1_s <= '0' ;
        incr_s  <= '0' ;    
    end PROCESS;
-------------------------------------------
end BEHAVIORAL;