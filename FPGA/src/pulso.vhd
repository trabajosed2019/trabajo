----------------------------------------------------------------------------------
-- Company	      : 	Grupo Simon Dice
-- Engineers	  : 	Borja Gutierrez de Cabiedes
--          	        Diego Machuca Meo�o
--           	        Sergio Martinez Plana
-- Design Name	  : 	Modulo Pulso
-- Module Name    : 	Modulo Pulso - BEHAVIORAL
-- Project Name   : 	Simon Dice
-- Target Devices : 	Artix 7 (Nexys 4DDR)
-- Tool Versions  : 	Vivado 2019.1
-- Description    : 
--              	    Se utiliza a modo de comprobacion del funcionamiento del sistema. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY pulso is
GENERIC (
  -- A 2 Hz heartbeat with 50% duty cycle on a 100 MHz clock
  COUNT_VAL : UNSIGNED(26 DOWNTO 0) := resize(d"25000000", 27)
);
PORT (
  clk   : in    STD_LOGIC ;
  pulso : out   STD_LOGIC
);
end pulso;

ARCHITECTURE BEHAVIORAL of pulso is
--Signals
  signal clk_s         : STD_LOGIC                              ;
--Present signals
  signal delay_cnt_p_s : UNSIGNED(26 DOWNTO 0) := (others => '0') ;
  signal pulso_p_s     : STD_LOGIC             := '0'             ;
--Future signals
  signal delay_cnt_f_s : UNSIGNED(26 DOWNTO 0)                    ;
  signal pulso_f_s     : STD_LOGIC                                ;
BEGIN
  clk_s <= clk ;
  -----------------------------------------------------------------
  REGISTERED: PROCESS (clk_s)
  BEGIN
    if rising_edge(clk_s) then
      delay_cnt_p_s <= delay_cnt_f_s ;
      pulso_p_s     <= pulso_f_s     ;
    end if;
  end PROCESS;
---------------------------------------------------------------------
  COMBINATORIAL: PROCESS (delay_cnt_p_s, pulso_p_s)
  BEGIN
    if delay_cnt_p_s = 0 then
      delay_cnt_f_s <= COUNT_VAL       ;
      -- Toggle heartbeat
      pulso_f_s     <= not pulso_p_s   ;
    else
      delay_cnt_f_s <= delay_cnt_p_s - 1 ;
      pulso_f_s     <= pulso_p_s         ;
    end if;
  end PROCESS;
  ----------------------------------------------------------------------------
  -- Assignment Outputs
  ----------------------------------------------------------------------------
  pulso <= pulso_p_s;

end BEHAVIORAL;