----------------------------------------------------------------------------------
-- Company	      : 	Grupo Simon Dice
-- Engineers	  : 	Borja Gutierrez de Cabiedes
--          	 	    Diego Machuca Meo�o
--           		    Sergio Martinez Plana
-- Design Name	  : 	Modulo Maquina de Estados (Display)
-- Module Name    : 	Modulo Maquina de Estados (Display) - BEHAVIORAL
-- Project Name   : 	Simon Dice
-- Target Devices : 	Artix 7 (Nexys 4DDR)
-- Tool Versions  : 	Vivado 2019.1
-- Description    : 
--              	    Establece un retardo al pausar y mostrar para que el jugador se
--              	    encuentre preparado para responder a esta situacion.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY st_machine_display is
PORT (
  rst           : in    STD_LOGIC                    ;
  clk           : in    STD_LOGIC                    ;
  player        : in    STD_LOGIC                    ;
  gameover      : in    STD_LOGIC                    ;
  digit_valid   : in    STD_LOGIC                    ;
  digit         : in    STD_LOGIC_VECTOR(1 DOWNTO 0) ;
  end_delay     : in    STD_LOGIC                    ;
  start_delay   : out   STD_LOGIC                    ;
  display_ready : out   STD_LOGIC                    ;
  md_player     : out   STD_LOGIC                    ;
  md_gameover   : out   STD_LOGIC                    ;
  md_serie      : out   STD_LOGIC_VECTOR(3 DOWNTO 0)
);
end st_machine_display;

ARCHITECTURE BEHAVIORAL of st_machine_display is
type t_state is (READY, HIGHLIGHT, WAIT_HG, PAUSE, WAIT_PAUSE);
-- Signals
  signal rst_s              : STD_LOGIC                    ;
  signal clk_s              : STD_LOGIC                    ;
-- Present signals
  signal start_delay_p    : STD_LOGIC                    ;
  signal display_ready_p  : STD_LOGIC                    ;
  signal md_player_p      : STD_LOGIC                    ;
  signal md_gameover_p    : STD_LOGIC                    ;
  signal md_serie_p       : STD_LOGIC_VECTOR(3 DOWNTO 0) ;
-- Future signals
  signal start_delay_f    : STD_LOGIC                    ;
  signal display_ready_f  : STD_LOGIC                    ;
  signal md_player_f      : STD_LOGIC                    ;
  signal md_gameover_f    : STD_LOGIC                    ;
  signal md_serie_f       : STD_LOGIC_VECTOR(3 DOWNTO 0) ;
-- Internal signals
  signal state_p, state_f : t_state                      ;

BEGIN
  rst_s <= rst ;
  clk_s <= clk ;
--------------------------------------------------------------------------------------------------
  REGISTERED: PROCESS (clk_s)
  BEGIN
    if rising_edge(clk_s) then
      if (rst_s = '1') then
        state_p         <= READY           ;
        start_delay_p   <= '0'             ;
        display_ready_p <= '1'             ;
        md_player_p     <= '0'             ;
        md_gameover_p   <= '0'             ;
        md_serie_p      <= (others => '0') ;
      else
        state_p         <= state_f         ;
        start_delay_p   <= start_delay_f   ;
        display_ready_p <= display_ready_f ;
        md_player_p     <= md_player_f     ;
        md_gameover_p   <= md_gameover_f   ;
        md_serie_p      <= md_serie_f      ;
      end if;
    end if;
  end PROCESS;
--------------------------------------------------------------------------------------------------
  COMBINATORIAL: PROCESS (state_p , display_ready_p, player, gameover, md_serie_p, 
                         digit_valid, digit, end_delay)
  BEGIN
    state_f         <= state_p         ;
    start_delay_f   <= '0'             ;  -- active a clock cycle 
    display_ready_f <= display_ready_p ;
    md_player_f     <= player          ;
    md_gameover_f   <= gameover        ;
    md_serie_f      <= md_serie_p      ;
-- READY -> HIGHLIGHT 
    case state_p is
      when READY      =>
        if digit_valid = '1' then
          state_f                                  <= HIGHLIGHT       ;
          start_delay_f                            <= '1'             ;
          display_ready_f                          <= '0'             ;
          -- bit highlighted
          -- sample digit when digit_valid
          md_serie_f(TO_INTEGER(UNSIGNED(digit)))  <= '1'             ; 
        end if;
-- HIGHLIGHT -> WAIT_HG
      when HIGHLIGHT  =>
        state_f                                    <= WAIT_HG         ;
-- WAIT_HG -> PAUSE
      when WAIT_HG    =>
        if end_delay = '1' then
          state_f                                  <= PAUSE           ;
          md_serie_f                               <= (others => '0') ;  -- end of highlight
        end if;
-- PAUSE -> WAIT_PAUSE
      when PAUSE =>
        state_f                                    <= WAIT_PAUSE      ;
        start_delay_f                              <= '1'             ;
-- WAIT_PAUSE -> READY
      when WAIT_PAUSE =>
        if end_delay = '1' then
          state_f                                  <= READY           ;
          display_ready_f                          <= '1'             ;
        end if;
      when others     =>
        state_f                                    <= READY           ;
        display_ready_f                            <= '1'             ;
    end case;
  end PROCESS;

-- Assignment of outputs
  start_delay   <= start_delay_p   ;
  display_ready <= display_ready_p ;
  md_player     <= md_player_p     ;
  md_gameover   <= md_gameover_p   ;
  md_serie      <= md_serie_p      ;

end BEHAVIORAL;