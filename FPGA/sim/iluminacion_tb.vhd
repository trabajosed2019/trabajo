library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
ENTITY iluminacion_tb is
-- PORT();
end iluminacion_tb;

ARCHITECTURE BEHAVIORAL of iluminacion_tb is
--Component
  component iluminacion is
   GENERIC (
    -- The brightness is defined by the number of active cycles and consequently the parameters are established. (25%)
    RATIO_16 : UNSIGNED(3 DOWNTO 0) := X"4" ;
    WIDTH    : INTEGER              := 4
   );
   PORT (
    clk         : in    STD_LOGIC                          ;
    serie       : in    STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0) ;
    lumi_serie  : out   STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0)
   );
  end component;
--Signals
  signal clk_s                 : STD_LOGIC                             ;
  signal serie_s, lumi_serie_s : STD_LOGIC_VECTOR(3 DOWNTO 0)          ;
--Constant
  constant clk_period          : TIME                         := 10 ns ;
BEGIN
   uut: iluminacion
    PORT MAP (
    clk        => clk_s        ,
    serie      => serie_s      ,
    lumi_serie => lumi_serie_s
    );
---------------------------------------------------------
   clk_gen: PROCESS
    BEGIN
        clk_s <= '1' ;
        wait for clk_period/2 ;
        clk_s <= '0' ;
        wait for clk_period/2 ;
    end PROCESS;
--------------------------------------------------------
   stimulus: PROCESS
    BEGIN
-- Comprobacion de secuencias
      serie_s <= "0000";
      wait for 16* clk_period ;
      serie_s <= "0001";
      wait for 16* clk_period ;
      serie_s <= "0010";
      wait for 16* clk_period ;
      serie_s <= "0011";
      wait for 16* clk_period ;
      serie_s <= "0100";
      assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
    end PROCESS;

end BEHAVIORAL;

