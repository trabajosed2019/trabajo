----------------------------------------------------------------------------------
-- Company	      : 	Grupo Simon Dice
-- Engineers	  : 	Borja Gutierrez de Cabiedes
--          	 	    Diego Machuca Meo�o
--           		    Sergio Martinez Plana
-- Design Name	  : 	Modulo Debouncer
-- Module Name    : 	Modulo Debouncer - BEHAVIORAL
-- Project Name   : 	Simon Dice
-- Target Devices : 	Artix 7 (Nexys 4DDR)
-- Tool Versions  : 	Vivado 2019.1
-- Description    : 
--              	    Los pulsadores tardan cierto tiempo en alcanzar un estado estable por 
--              	    eso en nuestro caso establecemos un filtro con un cte de 1 ms.  
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY dbnc is
PORT (
  rst      : in    STD_LOGIC ;
  clk      : in    STD_LOGIC ;
  sample   : in    STD_LOGIC ; 			 -- sample signal should be around 8 kHz
  sig      : in    STD_LOGIC ;
  dbnc_sig : out   STD_LOGIC
);
end dbnc;

ARCHITECTURE BEHAVIORAL of dbnc is
--Signals
  signal rst_s      : STD_LOGIC                    ;
  signal clk_s      : STD_LOGIC                    ;
  signal dbnc_reg_s : STD_LOGIC_VECTOR(7 DOWNTO 0) ;
 BEGIN
  rst_s <= rst ;
  clk_s <= clk ;

  REGISTERED: PROCESS(clk_s)
  begin
    if rising_edge(clk_s) then
      if (rst_s='1') then
        dbnc_reg_s <= (others => '0')              ;
      elsif (sample = '1') then
        dbnc_reg_s <= dbnc_reg_s(6 downto 0) & sig ;
      end if;

      if (dbnc_reg_s = X"FF") then
        dbnc_sig <= '1' ;
      else
        dbnc_sig <= '0' ;
      end if;
    end if; 
  end PROCESS;   
     
end BEHAVIORAL;