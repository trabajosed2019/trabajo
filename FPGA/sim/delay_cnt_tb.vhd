library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY delay_cnt_tb is
--  PORT ( );
end delay_cnt_tb;

ARCHITECTURE BEHAVIORAL of delay_cnt_tb is
--Component
 component delay_cnt is
 GENERIC (
   -- Default is 30M cycles, with a 100MHz clock, which is 300 ms
   SHORT_SIM       : BOOLEAN               := false                     ;
   COUNT_VAL       : UNSIGNED(24 DOWNTO 0) := TO_UNSIGNED(30000000, 25) ;
   COUNT_VAL_SHORT : UNSIGNED(24 DOWNTO 0) := TO_UNSIGNED(2, 25)
 );
 PORT (
   rst         : in    STD_LOGIC ;
   clk         : in    STD_LOGIC ;
   start_delay : in    STD_LOGIC ;
   end_delay   : out   STD_LOGIC
 );
 end component;
--Signals    
  signal rst_s, clk_s, start_delay_s : STD_LOGIC        ;
  signal end_delay_s                 : STD_LOGIC := '0' ;
--Constant
  constant clk_period                : TIME := 10 ns    ;    
BEGIN
    uut: delay_cnt
    PORT MAP (
        rst         => rst_s         ,
        clk         => clk_s         ,
        start_delay => start_delay_s ,
        end_delay   => end_delay_s
    );


-------------------------------------------------------------    
    clk_gen: PROCESS
    BEGIN
        clk_s <= '1' ;
        wait for clk_period/2 ;
        clk_s <= '0' ;
        wait for clk_period/2 ;
    end PROCESS;   
--------------------------------------------------------------    
 -- RESET a 0
    rst_s <= '1','0' after 3*clk_period;
--------------------------------------------------------------
    test: PROCESS
    BEGIN
-- Comprobador de start_delay
    start_delay_s <= '1';
    wait for clk_period;
    start_delay_s <= '0';    
    wait on end_delay_s until end_delay_s = '1' for 310ms;
    assert end_del = '1' report "ERROR: Delay counter" severity failure;
    end PROCESS;
--------------------------------------------------------------  
end BEHAVIORAL ;