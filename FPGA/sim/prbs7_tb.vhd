library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

ENTITY prbs7_tb is
--  PORT ( );
end prbs7_tb;

ARCHITECTURE BEHAVIORAL of prbs7_tb is
--Component
 component prbs7 is
 PORT (
   clk       : in    STD_LOGIC	                  ;
   load_prbs : in    STD_LOGIC	                  ;
   next_prbs : in    STD_LOGIC	                  ;
   seed      : in    STD_LOGIC_VECTOR(6 DOWNTO 0) ;
   digit     : out   STD_LOGIC_VECTOR(1 DOWNTO 0)
 );
 end component;
--Signals
  signal clk_s, load_prbs_s, next_prbs_s : STD_LOGIC := '0'                                 ;
  signal seed_s                          : STD_LOGIC_VECTOR (6 downto 0) := "1001000"       ;
  signal digit_s                         : STD_LOGIC_VECTOR (1 downto 0) := (others => '0') ;
--Constant
  constant clk_period                    : TIME                          := 10ns            ;
BEGIN
  uut: prbs7
    PORT MAP (
        clk       => clk_s       ,
        load_prbs => load_prbs_s ,
        next_prbs => next_prbs_s ,
        seed      => seed_s      ,
        digit     => digit_s  
    );
------------------------------------------------------------------------
  clk_gen: PROCESS
  BEGIN
       clk_s <= '1'            ; 
       wait for clK_period/2 ; 
       clk_s <= '0'            ; 
       wait for clk_period/2 ;
  end PROCESS;
----------------------------------------------------------------------
--Acyivacion del LOAD_PRBS
load_prbs_s <= '1', '0' after 5*clk_period;
-----------------------------------------------------------------------
  test: PROCESS
  BEGIN
    
     wait for 6*clk_period;
     assert digit_s = "00" 
     report "Incorrect Output" severity error;
    
     next_prbs_s <= '1' ;
     wait for clk_period;
     next_prbs_s <= '0' ;
     wait for clk_period;
    
     assert digit_s = "00" 
     report "Next takes effect after just 1 clock!" severity error;
    
     next_prbs_s <= '1' ;
     wait for 2*clk_period;
     next_prbs_s <= '0' ;
     wait for clk_period;
    
     assert digit_s = "01" 
     report "Incorrect Output" severity error;
    
     next_prbs_s <= '1';
     wait for 2*clk_period;
     next_prbs_s <= '0';
     wait for clk_period;
    
     assert digit_s = "10" 
     report "Incorrect Output" severity error;    
  end PROCESS;
-----------------------------------------------------------------------------
end BEHAVIORAL;