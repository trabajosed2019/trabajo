----------------------------------------------------------------------------------
-- Company	      : 	Grupo Simon Dice
-- Engineers	  : 	Borja Gutierrez de Cabiedes
--          	 	    Diego Machuca Meo�o
--           		    Sergio Martinez Plana
-- Design Name	  : 	Modulo Iluminacion
-- Module Name    : 	Modulo Iluminacion - BEHAVIORAL
-- Project Name   : 	Simon Dice
-- Target Devices : 	Artix 7 (Nexys 4DDR)
-- Tool Versions  : 	Vivado 2019.1
-- Description    : 
--              	    Control de brillo de los elementos.  
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY iluminacion is
GENERIC (
  -- The brightness is defined by the number of active cycles and consequently the parameters are established.
  RATIO_16 : UNSIGNED(3 DOWNTO 0) := X"2" ;
  WIDTH    : INTEGER              := 4
);
PORT (
  clk        : in    STD_LOGIC                          ;
  serie      : in    STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0) ;
  lumi_serie : out   STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0)
);
end iluminacion;
ARCHITECTURE BEHAVIORAL of iluminacion is
--Signals
  signal period : INTEGER := 0 ; 
BEGIN

-- Contador de periodos
-----------------------------------------------------------------
  counter: PROCESS(clk)     
  BEGIN  
    if rising_edge(clk) then
        period <= period + 1 ;        
         if period = 15 then
            period <= 0      ;
         end if;   
    end if;  
  end PROCESS;
------------------------------------------------------------------
-- Medidor de RATIO_16  
    power_on: PROCESS(period)
    BEGIN
        for i in 0 to WIDTH-1 loop
                if serie(i) = '0' then
                    if period < RATIO_16 then
                        lumi_serie (i) <= '1' ;
                     else
                        lumi_serie (i) <= '0' ;  
                    end if;
                 else
                     lumi_serie (i) <= '1';  
                 end if;
         end loop;
    end PROCESS;
-----------------------------------------------------------------    
end BEHAVIORAL;