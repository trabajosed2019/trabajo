library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

ENTITY st_machine_simon_tb is
--PORT ();
end st_machine_simon_tb;

ARCHITECTURE BEHAVIORAL of st_machine_simon_tb is
-- Components
component st_machine_simon is
PORT (
  rst              : in    STD_LOGIC                    ;
  clk              : in    STD_LOGIC                    ;
  start_game       : in    STD_LOGIC                    ;
  key_pressed      : in    STD_LOGIC                    ;
  key_correct      : in    STD_LOGIC                    ;
  serie_length     : in    STD_LOGIC_VECTOR(7 DOWNTO 0) ;
  item_cnt         : in    STD_LOGIC_VECTOR(7 DOWNTO 0) ;
  display_ready    : in    STD_LOGIC                    ;
  next_prbs        : out   STD_LOGIC                    ;
  load_prbs        : out   STD_LOGIC                    ;
  serie_length_1   : out   STD_LOGIC                    ;
  serie_length_inc : out   STD_LOGIC                    ;
  item_cnt_rst     : out   STD_LOGIC                    ;
  item_cnt_inc     : out   STD_LOGIC                    ;
  ms_player        : out   STD_LOGIC                    ;
  ms_gameover      : out   STD_LOGIC                    ;
  ms_digit_valid   : out   STD_LOGIC                    
);
end COMPONENT;
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
component cnt is
GENERIC (WIDTH : INTEGER := 8);
PORT (
  clk     : in    STD_LOGIC                          ;
  rst     : in    STD_LOGIC                          ;  					-- sincronizar
  set_1   : in    STD_LOGIC                          ;  					-- set 1
  incr    : in    STD_LOGIC                          ;  					-- incremento
  counter : out   STD_LOGIC_VECTOR(WIDTH-1 DOWNTO 0)
);
end component;    
--Signals
  signal rst_s              :     STD_LOGIC                    ; 
  signal clk_s              :     STD_LOGIC                    ;
  signal start_game_s       :     STD_LOGIC                    ;
  signal key_pressed_s      :     STD_LOGIC                    ;
  signal key_correct_s      :     STD_LOGIC                    ;
  signal serie_length_s     :     STD_LOGIC_VECTOR(7 DOWNTO 0) ;
  signal item_cnt_s         :     STD_LOGIC_VECTOR(7 DOWNTO 0) ;
  signal display_ready_s    :     STD_LOGIC                    ;
  signal next_prbs_s        :     STD_LOGIC                    ;
  signal load_prbs_s        :     STD_LOGIC                    ;
  signal serie_length_1_s   :     STD_LOGIC                    ;
  signal serie_length_inc_s :     STD_LOGIC                    ;
  signal item_cnt_rst_s     :     STD_LOGIC                    ;
  signal item_cnt_inc_s     :     STD_LOGIC                    ;
  signal ms_player_s        :     STD_LOGIC                    ;
  signal ms_gameover_s      :     STD_LOGIC                    ;
  signal ms_digit_valid_s   :     STD_LOGIC                    ;
--Constants
  constant clk_period        : TIME := 10 ns           ;
  constant button_press_time : TIME := 2 ms            ;
  constant display_wait_time : TIME := 6.2 ms          ;
  
BEGIN
 
-- Instantiations
  utt: st_machine_simon
   PORT MAP (   
    rst              => rst_s              ,
    clk              => clk_s              ,
    start_game       => start_game_s       ,
    key_pressed      => key_pressed_s      ,
    key_correct      => key_correct_s      ,
    serie_length     => serie_length_s     ,
    item_cnt         => item_cnt_s         ,
    display_ready    => display_ready_s    ,
    next_prbs        => next_prbs_s        ,
    load_prbs        => load_prbs_s        ,
    serie_length_1   => serie_length_1_s   ,
    serie_length_inc => serie_length_inc_s ,
    item_cnt_rst     => item_cnt_rst_s     ,
    item_cnt_inc     => item_cnt_inc_s     ,
    ms_player        => ms_player_s        ,
    ms_gameover      => ms_gameover_s      ,
    ms_digit_valid   => ms_digit_valid_s                    
  );
------------------------------------------------------------------------------------------------------
 serie_length_inst : cnt
  PORT MAP (
    clk     => clk_s              ,
    rst     => '0'                ,
    set_1   => serie_length_1_s   ,
    incr    => serie_length_inc_s ,
    counter => serie_length_s
  );
-------------------------------------------------------------------------------------------------------  
 item_cnt_inst : cnt
  PORT MAP (
    clk     => clk_s          ,
    rst     => item_cnt_rst_s ,
    set_1   => '0'            ,
    incr    => item_cnt_inc_s ,
    counter => item_cnt_s
  );
------------------------------------------------------------------------------------------------------
-- RESET
  rst_s <= '1', '0' after 20 ns ;
------------------------------------------------------------------------------------------------------
-- CLOCK  
  clk_gen: PROCESS
  BEGIN
     clk_s <= '1' ; 
     wait for clK_period/2 ;
     clk_s <= '0' ; 
     wait for clk_period/2 ;
  end PROCESS;
-------------------------------------------------------------------------------------------------------
  test: PROCESS
  BEGIN
  -- Se�ales a false
  start_game_s    <= '0' ;
  display_ready_s <= '0' ;
  key_pressed_s   <= '0' ;
  key_correct_s   <= '0' ;

  -- Inicio
  wait for 20 ns      ;
  start_game_s <= '1'   ;
  wait for clk_period ;
  start_game_s <= '0'   ;

  -- Preparar
  wait for clk_period  ;
  display_ready_s <= '1' ;

  -- Comprobar Secuancia 
  wait for 2*clk_period       ;

  -- Turno Jugador
  wait for 2*clk_period ;
  key_pressed_s <= '1'    ;

  -- Comprobar entrada
  wait for 2*clk_period ;
  key_correct_s <= '1'    ;
  key_pressed_s <= '0'    ;
  wait for 2*clk_period ;
  key_correct_s <= '0'    ;

-- Comprobar secuencia del jugador
  wait for 4*clk_period ;

-- Nuevo elemento

-- Fin
  assert false
  report "[SUCCESS]: simulation finished OK."
  severity failure;
 end PROCESS;

end BEHAVIORAL;
