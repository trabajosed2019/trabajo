library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY SIMON_TOP_tb is
-- PORT();
end SIMON_TOP_tb;

ARCHITECTURE BEHAVIORAL of SIMON_TOP_tb is

component SIMON_TOP
  GENERIC ( SHORT_SIM : BOOLEAN := false);
  PORT (
    rst_n       : in    STD_LOGIC                    ;
    clk_100mhz  : in    STD_LOGIC                    ;
    start_game  : in    STD_LOGIC                    ;
    btn         : in    STD_LOGIC_VECTOR(3 downto 0) ;
    seed        : in    STD_LOGIC_VECTOR(6 downto 0) ;
    leds        : out   STD_LOGIC_VECTOR(7 downto 0)
  );
end component;
--Signals
  signal rst_s                 : STD_LOGIC                                        ;
  signal rst_n_s               : STD_LOGIC                                        ;
  signal clk_100mhz_s          : STD_LOGIC                                        ;
  signal start_game_s          : STD_LOGIC                     := '0'             ;
  signal btn_s                 : STD_LOGIC_VECTOR (3 downto 0) := (others => '0') ;
  signal seed_s                : STD_LOGIC_VECTOR (6 downto 0)                    ;
  signal leds_s                : STD_LOGIC_VECTOR (7 downto 0)                    ;
  signal enable_clk_src        : BOOLEAN := true                                  ;
--Constants
  constant clk_period          : TIME                          := 10 ns           ;
  constant button_press_time   : TIME                          := 2 ms            ;
  constant display_wait_time   : TIME                          := 6.2 ms          ;

BEGIN

  uut: SIMON_TOP
  GENERIC MAP (SHORT_SIM => true)
    PORT MAP (
    clk_100mhz => clk_100mhz_s ,
    rst_n      => rst_n_s      ,
    start_game => start_game_s ,
    btn        => btn_s        ,
    seed       => seed_s       ,
    leds       => leds_s
  );

-- RESETS
  rst_n_s  <= not rst_s            ;
  rst_s    <= '1', '0' after 52 ns ;
-- CLOCK
  clocking: PROCESS
  BEGIN
    while enable_clk_src loop
      clk_100mhz_s <= '0', '1' after clk_period / 2 ;
      wait for clk_period                           ;
    end loop;
    wait;
  end PROCESS;
  --------------------------------------------------------------------------------------------------------------------------------
  stimulus: PROCESS
  BEGIN

    -- Wait until reset
    seed_s      <= "1001000"     ; 
    wait for 13 * clk_period   ;
    report "Start a 1� game"   ; 
    start_game_s <= '1'          ;
    wait for button_press_time ;    
    start_game_s <= '0' ;    
    assert leds_s(3 downto 0) = "0001" report "Error PRBS7 output" severity error     ;    
    assert leds_s(6)          = '1' report "Error Md_player_o value" severity error   ;
    wait for display_wait_time ;
    
    -- Serie 1
    assert leds_s(6) = '1' report "Error player mode output serie 1" severity error     ;
    btn_s(0) <= '1' ;
    wait for button_press_time   ;        
    assert leds_s(3 downto 0) = "0001" report "Encoder Error serie 1" severity error    ;      
    btn_s(0) <= '0' ;    
    wait for button_press_time   ;
    assert leds_s(6) = '0' report "Error generation mode output serie 1" severity error ;
    wait for 2*display_wait_time ;
    
    -- Serie 2
    assert leds_s(6) = '1' report "Error player mode output serie 2" severity error     ;
    btn_s(0) <= '1' ;
    wait for button_press_time ;     
    assert leds_s(3 downto 0) = "0001" report "Encoder Error serie 2" severity error    ;    
    btn_s(0) <= '0' ;
    wait for button_press_time ;
    btn_s(1) <= '1' ;
    wait for button_press_time ;
    assert leds_s(3 downto 0) = "0100" report "Encoder Error serie 2" severity error    ;     
    btn_s(1) <= '0' ;   
    wait for button_press_time ;
    assert leds_s(6) = '0' report "Error generation mode output serie 2" severity error ;
    wait for 3*display_wait_time;
    
    -- Serie 3
    assert leds_s(6) = '1' report "Error player mode output serie 3" severity error     ;   
    btn_s(0) <= '1' ;
    wait for button_press_time ;
    assert leds_s(3 downto 0) = "0001" report "Encoder Error serie 3" severity error    ;
    btn_s(0) <= '0' ;
    wait for button_press_time ;
    btn_s(1) <= '1' ;
    wait for button_press_time ;
    assert leds_s(3 downto 0) = "0100" report "Encoder Error serie 3" severity error    ;   
    btn_s(1) <= '0' ;
    wait for button_press_time ;
    btn_s(2) <= '1' ;
    wait for button_press_time ;
    assert leds_s(3 downto 0) = "1000" report "Encoder Error serie 3" severity error    ; 
    btn_s(2) <= '0' ;
    wait for button_press_time ;
    
    assert leds_s(6) = '0' report "Error generation mode output serie 3" severity error ;
    wait for 4*display_wait_time;

    -- Serie 4
    assert leds_s(6) = '1' report "Error player mode output" severity error           ;
    btn_s(0) <= '1' ;
    wait for button_press_time ;
    assert leds_s(3 downto 0) = "0001" report "Encoder Error serie 4" severity error    ;
    btn_s(0) <= '0' ;
    wait for button_press_time ;
    
    -- Serie 4 ERROR
    leds_s(0) <= '1' ;
    wait for button_press_time ;    
    assert leds_s(3 downto 0) = "0001" report "Encoder Error serie 3" severity error    ; 
    btn_s(0) <= '0' ;
    wait for button_press_time ;
    assert leds_s(5) = '1' report "Error md_gameover output" severity error           ;

    report "End of simulation." ;
    -- Stop the clock to end the simulation automatically
    enable_clk_src <= false ;
    wait ;    
  end PROCESS;
  
end BEHAVIORAL;

