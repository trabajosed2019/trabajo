library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

ENTITY pulso_tb is
--PORT();
end pulso_tb;

ARCHITECTURE BEHAVIORAL of pulso_tb is
--Component
 component pulso is
 GENERIC (
   -- A 2 Hz heartbeat with 50% duty cycle on a 100 MHz clock
   COUNT_VAL : UNSIGNED(26 DOWNTO 0) := resize(d"25000000", 27)
 );
 PORT (
   clk        : in    STD_LOGIC;
   pulso  : out   STD_LOGIC
 );
 end component;
--Signals
  signal clk_s, pulso_s : STD_LOGIC          ;
--Constant
  constant clk_period       : TIME      := 10 ns ;
BEGIN
    uut: pulso
     PORT MAP (
         clk   => clk_s    ,
         pulso => pulso_s
    );
----------------------------------------------------------
   clk_gen: PROCESS
    BEGIN
        clk_s <= '1' ;
        wait for clk_period/2 ;
        clk_s <= '0' ;
        wait for clk_period/2 ;
    end PROCESS;
----------------------------------------------------------
end BEHAVIORAL;