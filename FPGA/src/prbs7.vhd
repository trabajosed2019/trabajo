----------------------------------------------------------------------------------
-- Company	       : 	Grupo Simon Dice
-- Engineers	   : 	Borja Gutierrez de Cabiedes
--          	 	    Diego Machuca Meo�o
--           		    Sergio Martinez Plana
-- Design Name	   : 	Modulo PRBS7
-- Module Name     : 	Modulo PRBS7 - BEHAVIORAL
-- Project Name    : 	Simon Dice
-- Target Devices  : 	Artix 7 (Nexys 4DDR)
-- Tool Versions   : 	Vivado 2019.1
-- Description     : 
--              	    Generador binario de pseudocodigo a trav�s de una semilla.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

ENTITY prbs7 is
PORT (
  clk       : in    STD_LOGIC			   ;
  load_prbs : in    STD_LOGIC			   ;
  next_prbs : in    STD_LOGIC			   ;
  seed      : in    STD_LOGIC_VECTOR(6 DOWNTO 0) ;
  digit    : out   STD_LOGIC_VECTOR(1 DOWNTO 0)
);
end prbs7;

ARCHITECTURE BEHAVIORAL of prbs7 is
--Signal
    signal b : STD_LOGIC_VECTOR (6 DOWNTO 0):= (others => '0') ;                         
BEGIN  
  PROCESS (clk, load_prbs, next_prbs)
    variable coups_clk: INTEGER range 0 to 2 := 0 ; 
    BEGIN
      if rising_edge(clk) then        
         if(load_prbs = '1') then
            b         <= seed ;
            coups_clk := 0    ;
         else            
            if next_prbs = '1' then
                coups_clk := coups_clk + 1 ;
                if coups_clk = 2 then
                    b         <=  b(5 DOWNTO 0) & (b(6) xor b(5)) ;                    
                    coups_clk := 0                                ;
                end if;  
                         
            else 
                  coups_clk := 0 ;            
         end if;  
       end if;    
    end if;    
  end PROCESS;

-- Asignment Output
digit <= b(1 DOWNTO 0) ; 
 
end BEHAVIORAL;
